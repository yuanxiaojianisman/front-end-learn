import { activeEffect, trackEffect } from "./effect";

const targetMap = new WeakMap();
function track(target, key) {
  // 依赖 属性 键
  if (activeEffect) {
    let depsMap = targetMap.get(target);
    console.log(depsMap);

    if (!depsMap) {
      targetMap.set(target, (depsMap = new Map()));
    }
    console.log(depsMap);

    let dep = depsMap.get(key);
    console.log(dep);

    console.log(key);

    if (!dep) {
      depsMap.set(key, (dep = createDep(() => depsMap.delete(key))));
    }
    trackEffect(dep,activeEffect)

    console.log(targetMap);
  }
}
function createDep(cleanup) {
  const map = new Map() as any;
  map._cleanup = cleanup;
  console.log(map, "map complish");

  return map;
}
export { track ,targetMap};
