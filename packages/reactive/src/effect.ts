import { targetMap } from "./reactiveEffect";

function effect(Fn, option?) {
  const _effect = new ReactiveEffect(Fn, () => {
    _effect.run();
  });
  _effect.run();
}
let activeEffect;
class ReactiveEffect {
  trackId = 0;
  dep_length = 0;
  deps = [];
  public active = true;
  constructor(public Fn, public scheduler) {}
  run() {
    if (!this.active) {
      return this.Fn();
    }
    let lastEffect = activeEffect;
    try {
      activeEffect = this;
      console.log(activeEffect);

      return this.Fn();
    } finally {
      activeEffect = lastEffect;
    }
  }
}
function trackEffect(dep, effect) {
  dep.set(effect, effect.trackId);
  effect.deps[effect.dep_length++] = dep;
  console.log(effect.deps);
}
function trigger(target, key, value, oldValue) {
  let depMaps = targetMap.get(target);
  if (!depMaps) {
    return;
  }
  let dep = depMaps.get(key);
  if (dep) {
    triggerEffect(dep);
  }
}
function triggerEffect(dep) {
  for (const effect of dep.keys()) {
    if (effect.scheduler) {
      effect.scheduler();
    }
  }
}
export { effect, activeEffect, trackEffect, trigger };
