import { isObject } from "@vue/share";
import { mutableHandlers, reactiveFlags } from "./baseHandler";

// 接收要进行响应式的对象
function reactive(target) {
  return createReactiveObject(target);
}
export { reactive };
const reactiveMap = new WeakMap();

function createReactiveObject(target) {
  //是否是对象
  if (!isObject(target)) {
    return "not a object";
  }
  //是否有缓存
  const existProxy = reactiveMap.get(target);
  if (existProxy) {
    return existProxy;
  }
  //是否被代理过了
  if (target[reactiveFlags.IS_REACTIVE]) {
    return target;
  }
  let proxy = new Proxy(target, mutableHandlers);
  reactiveMap.set(target, proxy);
  console.log("beidaili");

  return proxy;
}
