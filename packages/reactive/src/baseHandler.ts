import { activeEffect, trigger } from "./effect";
import { track } from "./reactiveEffect";

enum reactiveFlags {
  IS_REACTIVE = "_isReactive",
}

const mutableHandlers: ProxyHandler<any> = {
  get(target, key, receiver) {
    if (key === reactiveFlags.IS_REACTIVE) {
      return true;
    }
    track(target, key); //依赖收集

    //当取值的时候应该让响应式属性和对应的effect映射起来

    return Reflect.get(target, key, receiver);
  },

  set(target, key, value, receiver) {
    //找到对应的effct 让页面更新
    const oldValue = target[key];
    const result = Reflect.set(target, key, value, receiver);
    
    
    if (oldValue !== value) {
      //需要触发更新
      trigger(target, key, value, oldValue);
    }

    return result;
  },
};

export { mutableHandlers, reactiveFlags };
