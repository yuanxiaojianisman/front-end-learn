// packages/share/src/index.ts
function isObject(value) {
  return typeof value === "object" && value !== null;
}

// packages/reactive/src/reactiveEffect.ts
var targetMap = /* @__PURE__ */ new WeakMap();
function track(target, key) {
  if (activeEffect) {
    let depsMap = targetMap.get(target);
    console.log(depsMap);
    if (!depsMap) {
      targetMap.set(target, depsMap = /* @__PURE__ */ new Map());
    }
    console.log(depsMap);
    let dep = depsMap.get(key);
    console.log(dep);
    console.log(key);
    if (!dep) {
      depsMap.set(key, dep = createDep(() => depsMap.delete(key)));
    }
    trackEffect(dep, activeEffect);
    console.log(targetMap);
  }
}
function createDep(cleanup) {
  const map = /* @__PURE__ */ new Map();
  map._cleanup = cleanup;
  console.log(map, "map complish");
  return map;
}

// packages/reactive/src/effect.ts
function effect(Fn, option) {
  const _effect = new ReactiveEffect(Fn, () => {
    _effect.run();
  });
  _effect.run();
}
var activeEffect;
var ReactiveEffect = class {
  constructor(Fn, scheduler) {
    this.Fn = Fn;
    this.scheduler = scheduler;
    this.trackId = 0;
    this.dep_length = 0;
    this.deps = [];
    this.active = true;
  }
  run() {
    if (!this.active) {
      return this.Fn();
    }
    let lastEffect = activeEffect;
    try {
      activeEffect = this;
      console.log(activeEffect);
      return this.Fn();
    } finally {
      activeEffect = lastEffect;
    }
  }
};
function trackEffect(dep, effect2) {
  dep.set(effect2, effect2.trackId);
  effect2.deps[effect2.dep_length++] = dep;
  console.log(effect2.deps);
}
function trigger(target, key, value, oldValue) {
  let depMaps = targetMap.get(target);
  if (!depMaps) {
    return;
  }
  let dep = depMaps.get(key);
  if (dep) {
    triggerEffect(dep);
  }
}
function triggerEffect(dep) {
  for (const effect2 of dep.keys()) {
    if (effect2.scheduler) {
      effect2.scheduler();
    }
  }
}

// packages/reactive/src/baseHandler.ts
var mutableHandlers = {
  get(target, key, receiver) {
    if (key === "_isReactive" /* IS_REACTIVE */) {
      return true;
    }
    track(target, key);
    return Reflect.get(target, key, receiver);
  },
  set(target, key, value, receiver) {
    const oldValue = target[key];
    const result = Reflect.set(target, key, value, receiver);
    if (oldValue !== value) {
      trigger(target, key, value, oldValue);
    }
    return result;
  }
};

// packages/reactive/src/reactive.ts
function reactive(target) {
  return createReactiveObject(target);
}
var reactiveMap = /* @__PURE__ */ new WeakMap();
function createReactiveObject(target) {
  if (!isObject(target)) {
    return "not a object";
  }
  const existProxy = reactiveMap.get(target);
  if (existProxy) {
    return existProxy;
  }
  if (target["_isReactive" /* IS_REACTIVE */]) {
    return target;
  }
  let proxy = new Proxy(target, mutableHandlers);
  reactiveMap.set(target, proxy);
  console.log("beidaili");
  return proxy;
}
export {
  activeEffect,
  effect,
  reactive,
  trackEffect,
  trigger
};
//# sourceMappingURL=reactive.js.map
