import { log } from "console";
import minimist from "minimist";
import esbuild from "esbuild";
import { dirname, resolve } from "path";
import { fileURLToPath } from "url";
import { createRequire } from "node:module";


const args = minimist(process.argv.slice(2));
const target = args._[0] || "reactive";
const format = args.f || "esm";
console.log(target,format)
const filename = fileURLToPath(import.meta.url);
const dir_name = dirname(filename);

const entry = resolve(dir_name, `../packages/${target}/src/index.ts`);
const outfile = resolve(dir_name, `../packages/${target}/dist/${target}.js`);

const require = createRequire(import.meta.url)
const pkg = require(`../packages/${target}/package.json`);
log(pkg)
esbuild.context({
  entryPoints: [entry],
  outfile,
  bundle: true,
  sourcemap: true,
  format,
  platform: "browser",
  globalName: pkg.compilerOption?.name,
  
}).then((result)=>{
    log('start dev and watching...')
   return result.watch()
});
